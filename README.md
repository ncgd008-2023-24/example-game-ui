# Gamejam Skeleton

This project is meant to be a starting point for use at gamejam.

It is meant as an already set up project without any precode gameplay stuff. There might be some general infrastructure parts, but other than that it should be a blank paper with all drawing equipment ready to use.

## [Wiki](https://gitlab.com/trampod/gamejam-skeleton/-/wikis/home)

There is a more detailed wiki explaining this project in greater detail.


## [Content (included packages)](https://gitlab.com/trampod/gamejam-skeleton/-/wikis/included-packages)

List of all imported packages used in this Game Jam Skeleton.

### [DOTween](https://gitlab.com/trampod/gamejam-skeleton/-/wikis/Packages/DOTween)

Library for doing [tweens](https://en.wikipedia.org/wiki/Inbetweening) in Unity. It allows you to do simple animations of various objects directly from code. Official documentation can be found [here](https://dotween.demigiant.com/documentation.php).

### [Execution Order Attribute](https://gitlab.com/trampod/gamejam-skeleton/-/wikis/Packages/Execution-Order-Attribute)

C# class attribute for MonoBehaviours for a more user-friendly [script execution order setting](https://docs.unity3d.com/Manual/class-MonoManager.html). Official Asset Store page can be found [here](https://assetstore.unity.com/packages/tools/utilities/execution-order-attributes-105354).

### [(New) Input System](https://gitlab.com/trampod/gamejam-skeleton/-/wikis/Packages/New-Input-System)

Unity package for more control over user input, allowing multiple different input devices at the same time, input processing and many more. Official documentation can be found [here](https://docs.unity3d.com/Packages/com.unity.inputsystem@1.6/manual/index.html).

### [TextMesh Pro](https://gitlab.com/trampod/gamejam-skeleton/-/wikis/Packages/TextMesh-Pro)

Advanced UI elements that form text as a mesh instead of a bitmap, allowing for more detail and clear edges. Official documentation can be found [here](https://docs.unity3d.com/Packages/com.unity.textmeshpro@3.2/manual/index.html).

### [MyBox](https://gitlab.com/trampod/gamejam-skeleton/-/wikis/Packages/MyBox)

Various Unity extensions and features. Mainly for the inspector. Repo with documentation can be found [here](https://github.com/Deadcows/MyBox).

### [Cinemachine](https://gitlab.com/trampod/gamejam-skeleton/-/wikis/Packages/Cinemachine)

Package for more advanced and codeless manipulation of cameras. Official webpage can be found [here](https://unity.com/unity/features/editor/art-and-design/cinemachine), while official documentation can be found [here](https://docs.unity3d.com/Packages/com.unity.cinemachine@2.3/manual/index.html).

### [Recorder](https://gitlab.com/trampod/gamejam-skeleton/-/wikis/Packages/Recorder)

Tool for making gameplay recordings directly in the Unity Editor. Official documentation can be found [here](https://docs.unity3d.com/Packages/com.unity.recorder@2.0/manual/index.html).

### [Utils](https://gitlab.com/trampod/gamejam-skeleton/-/wikis/utils)

Custom additions to the skeleton, that are not part of any package.

#### [Singleton\<T>](https://gitlab.com/trampod/gamejam-skeleton/-/wikis/Singleton)

Custom singleton with advanced features compared to the one in MyBox package.

#### [Extensions and helpers](https://gitlab.com/trampod/gamejam-skeleton/-/wikis/Extensions-And-Helpers)

There are various custom extensions and helpers to make some common actions easier.

- ArrayExtension
- ColorExtension
- ColorHelper
- IEnumerableExtension
- ListExtension
- RandomHelper
- VectorExtension

## Contributors

<!-- omit in toc -->
### Trampod (Original Creator)

[itch.io](https://trampod.itch.io)

<!-- omit in toc -->
### MianenCZ

[mianen.cz](https://mianen.cz)

[itch.io](https://mianencz.itch.io)

[buy me a coffee](https://www.buymeacoffee.com/MianenCZ)

<!-- omit in toc -->
### Quanti s.r.o.

[<img src="https://gitlab.com/trampod/gamejam-skeleton/-/wikis/Resources/quanti-logo.png" alt="quanti.cz" width="300"/>](https://www.quanti.cz/)

[quanti.cz](https://www.quanti.cz/)

