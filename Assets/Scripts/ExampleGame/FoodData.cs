using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FoodData", menuName = "ScriptableObjects/FoodDataScriptableObject", order = 1)]
public class FoodData : ScriptableObject
{
    public string ReadableName;
    public float Saturation;
    public GameObject Prefab;

    [Tooltip("Area where food can randomly spawn")]
    public WorldArea3 SpawningArea;
    
    public FoodScript Spawn()
    {
        var newFood = GameObject.Instantiate(Prefab);
        newFood.transform.position = SpawningArea.GetRandomPoint();
        newFood.transform.localEulerAngles = new Vector3(0f, 0f, Random.Range(0f, 360f));
        var script = newFood.AddComponent<FoodScript>();
        script.Data = this;
        return script;
    }
}
