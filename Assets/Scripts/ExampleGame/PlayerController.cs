using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float Speed = 1f;
    public float Saturation = 10f;

    private Vector3 _inputVector;

    private void Start()
    {
        StartCoroutine(SaturationTicks());
    }

    private void Update()
    {
        if(GameplayManager.Instance.IsInProgress)
        {
            UpdateInputs();
            UpdateLogic();
        }
    }
    
    private void OnDrawGizmos()
    {
        var controll = _inputVector;

        if (controll != Vector3.zero)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawLine(transform.position, transform.position + (controll * Speed));
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.TryGetComponent<FoodScript>(out var food))
        {
            Saturation += food.Data.Saturation;
            Saturation = Mathf.Clamp(Saturation, 0, 100);
            Destroy(food.gameObject);
            Debug.Log($"Saturation: {Saturation}");
        }
    }
    
    private void UpdateInputs()
    {
        transform.localPosition += new Vector3(_inputVector.x, _inputVector.y) * Speed * Time.deltaTime;
    }

    private void UpdateLogic()
    {
        if(Saturation < 0)
        {
            Destroy(gameObject);
        }
    }

    private IEnumerator SaturationTicks()
    {
        while(true)
        {
            yield return new WaitForSeconds(1f);
            Saturation -= 1f;
            Debug.Log($"Saturation: {Saturation}");
        }
    }

    public void OnMovement(UnityEngine.InputSystem.InputAction.CallbackContext context)
    {
        _inputVector = context.ReadValue<Vector2>();
    }
}
