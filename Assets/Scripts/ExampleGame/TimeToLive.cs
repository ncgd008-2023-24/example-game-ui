using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecutionOrder(-150)]
public class TimeToLive : MonoBehaviour
{
    public float RemainingTime;

    void Update()
    {
        RemainingTime -= Time.deltaTime;
        if(RemainingTime < 0)
            Destroy(gameObject);
    }
}
