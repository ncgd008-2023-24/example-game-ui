using MyBox;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

[ExecuteBefore(typeof(PlayerController))]
public class GameplayManager : UnityEngine.Singleton<GameplayManager>
{
    // API

    public bool IsRunning => _IsRunning;
    public bool IsFinished => _IsFinished;
    public bool IsInProgress => _IsRunning && !_IsFinished;
    public float RemainingTime
    {
        get
        {
            if (!IsRunning)
                return 0;
            return _GameLength - (Time.time - _StartTime);
        }
    }

    // INSPECTOR

    [SerializeField, ReadOnly]
    private bool _IsRunning = false;
    [SerializeField, ReadOnly]
    private bool _IsFinished = false;
    [SerializeField]
    private bool _StartOnEnable = true;
    [SerializeField, Tooltip("Time of one level in seconds")]
    private float _GameLength = 15f;

    public UnityEvent OnLevelStart;
    public UnityEvent OnLevelFinish;
    public UnityEvent<string> Test;




    #region public UnityEvent<float> SomeNiceEvent;

    /// <summary>
    /// This is documentation
    /// </summary>
    [Tooltip("This is documentation")]
    [SerializeField]
    [FormerlySerializedAs("qtim_SomeNiceEvent")]
    private UnityEvent<float> SomeNiceEvent;

    /// <summary>
    /// This is documentation
    /// </summary>
    public event System.EventHandler<float> OnSomeNiceEvent;

    /// <summary>
    /// Invokes <see cref="SomeNiceEvent"/> and <see cref="OnSomeNiceEvent"/>.
    /// </summary>
    /// <remarks>
    /// This is documentation
    /// </remarks>
    protected void InvokeSomeNiceEvent(float args)
    {
        if (SomeNiceEvent != null)
            SomeNiceEvent.Invoke(args);

        if (OnSomeNiceEvent != null)
            OnSomeNiceEvent.Invoke(this, args);
    }

    #endregion





    // RUNTIME PRIVATE FIELDS

    private float _StartTime;

    // Start is called before the first frame update
    private void OnEnable()
    {
        InvokeSomeNiceEvent(1f);
        Debug.Log("", gameObject);
        StartGame();
    }

    public void StartGame()
    {
        if (_StartOnEnable)
            StartCoroutine(StartCountdown());
    }

    IEnumerator StartCountdown()
    {
        Debug.Log("3");
        yield return new WaitForSeconds(1);
        Debug.Log("2");
        yield return new WaitForSeconds(1);
        Debug.Log("1");
        yield return new WaitForSeconds(1);
        Debug.Log("GO");
        OnLevelStart.Invoke();
        _StartTime = Time.time;
        _IsRunning = true;
        yield return new WaitForSeconds(_GameLength);
        _IsFinished = true;
    }
}
