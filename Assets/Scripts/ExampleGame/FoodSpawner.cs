using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class FoodSpawner : MonoBehaviour
{
    public List<FoodData> FoodDatas;
    public int DefaultFoodCount = 10;

    private List<FoodScript> allFoods;



    // Start is called before the first frame update
    IEnumerator Start()
    {
        allFoods = new();

        for (int i = 0; i < DefaultFoodCount; i++)
        {
            Spawn();
        }

        yield return new WaitUntil(() => GameplayManager.Instance.IsInProgress);

        while (GameplayManager.Instance.IsInProgress)
        {
            yield return new WaitForSeconds(Random.Range(0.2f, 1f));
            Spawn();
        }

        void Spawn(){
            var foodTemplate = FoodDatas.GetRandom(); //gets random item from list
            var newFood = foodTemplate.Spawn();
            allFoods.Add(newFood);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
