using UnityEngine;

/// <summary>
/// Represents Cubic floating point area defined by 2 corner points
/// </summary>
[System.Serializable]
public struct WorldArea3
{
    public WorldArea3(Vector3 from, Vector3 to)
    {
        this.From = from; 
        this.To = to;
    }

    /// <summary>
    /// First corner of cubic space
    /// </summary>
    [Tooltip("First corner of cubic space")]
    public Vector3 From;

    /// <summary>
    /// Second corner of cubic space
    /// </summary>
    [Tooltip("Second corner of cubic space")]
    public Vector3 To;

    public readonly Vector3 GetRandomPoint()
    {
        return new Vector3(
            Random.Range(From.x, To.x),
            Random.Range(From.y, To.y),
            Random.Range(From.z, To.z)
        ); 
    }
}